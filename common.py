
import urllib.parse
import base64
from Crypto.Cipher import AES
from urllib.parse import unquote

API_KEY = "punyatest"
SECURITY_KEY = "@8kRRd1itO9662!&"
APP_ID = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHAiOjEsImZmIjoiVyIsImJkIjoid2ViLXBjIiwibmJmIjoxNjc2NjEwMjgxLCJzcmMiOiJlbXRtdyIsImF2IjoiMS4wLjAuNCIsImFwcGlkIjoiNGZlNjhiNzUzNjc4NGUzNDA3YzNlY2YxOWJlN2M0YWQiLCJpc3MiOiJlbXQiLCJleHAiOjE2NzY2NTg2MDAsImlhdCI6MTY3NjYxMDU4MX0.PJ2JUJTQYuGRwOjGymnKzLXp9mDA9TPXExPuy1Rlma4"
USER_ID = 45742265
USER_PASSWORD = "AlgoSuccess!"
VENDOR_NAME = "punyatest"
# BASE_URL = f"https://np.nuvamawealth.com/edelmw-login/login/accounts/loginvendor/{VENDOR_NAME}"
BASE_URL = "https://np.nuvamawealth.com"
ORDER_SOURCE = API_KEY
ORDER_SOURCE_TOKEN = "42716893b1c816079a55f6dbf5dd35ce"


def common_header():
    return ""


def get_encr_token(url):
    query_pairs = {}
    query = urllib.parse.urlparse(url).query
    pairs = query.split("&")
    for pair in pairs:
        idx = pair.find("=")
        query_pairs[urllib.parse.unquote(pair[:idx])] = urllib.parse.unquote(pair[idx + 1:])
    return query['encrTkn']


unpad = lambda x: x[:-ord(x[-1])]


def decrypt_encr_token(vendor_password, vendor_aut_token, encrypted_token):
    key = vendor_password + vendor_aut_token[-16:]

    decoded_token = base64.b64decode(encrypted_token)

    decrypter = AES.new(key.encode(), AES.MODE_CBC, bytes(bytearray(16)))

    decrypted_token = decrypter.decrypt(decoded_token)

    return unpad(decrypted_token.decode())
