import requests as req
from urllib.parse import unquote
from common import BASE_URL, \
    SECURITY_KEY, API_KEY, VENDOR_NAME, \
    ORDER_SOURCE, get_encr_token, decrypt_encr_token, USER_PASSWORD, USER_ID


def get_order_source_token(base_url, vendor_name, security_key):
    api_url = f"{base_url}/edelmw-login/login/accounts/loginvendor/{vendor_name}"
    header = {
        "Content-Type": "application/json",
        "source": "punyatest"
    }
    res = req.post(api_url, headers=header, json={"pwd": security_key})

    return res.json()["message"]


# order_source_token = get_order_source_token(BASE_URL, VENDOR_NAME, SECURITY_KEY)
order_source_token = "42716893b1c816079a55f6dbf5dd35ce"
# redirect_url = f"https://nuvamawealth.com/login?ordsrc={ORDER_SOURCE}&ordsrctkn={order_source_token}&state=test123"

# print(redirect_url)

call_back_url = "https://127.0.0.1/?login_success=true&userid=45742265&state=test123&encrTkn=hG8GuS1mxHKUT%2Bjd%2F6qY2i7ZuOLap%2F4qTVfTsRDsTRw2xY1t%2FNAYu1Fr2szmxniylRrAvD%2Bdn562D%2BrB4GQiRIoF2JX6cMJJGbBQgOhBVspyO4sZ%2F0pELuoSxCrZDJ7v%2B8SEBSKAqxf7iBJZoE15sKbBbtzUN7VDptD64xt0cz2DDLPr%2BO0iZb3te1j5SLK3lklTlJYvA05WYS9uID%2FU%2BDUWLACqH%2FqRc9zmfTdnHexN3sFQao42kKiZizIPd1cvcx8lDes1Jw8QJBdSe2df7ZQnQUcUnC4ClF%2Fedm%2BeSeGymxnHaBkNkmw3mY1lp0W1sPY36xFCafZvZguP9GpnMrCG2dV%2FJQ50hde3ymzuSLqeWPwD8jT08a1eipNuW6ezuPfWrxNTzjZCLRldlMV4FiZ1d4Ka9kUF1Pl63xmxEiQWgR4tvHElab78FPs5VkAWNB6h3RIC8CAnQYlC5zxurZF%2B5dqIy530TfvxB5qL%2FXw%3D"

# encrypted_user_token = get_encr_token(call_back_url)

# access_token = decrypt_encr_token(SECURITY_KEY, order_source_token, unquote(encrypted_user_token))
access_token = "BE215553EA35D15B05577B9C0A05C23F7C45C656FCA405EEBEBAF1150639D19FBE51C3E576DD816BEE66BE95E9D8F8FABA4D24C10A18D4E6482ECFFE431B88637F101EFE1A60DD9C73A2D887ED490648E071282DFC84B286368BC3D397D726D6223D74046A68F9ADAB20A0BC795BD515B59B2617D8438646F42DD0A7CC2674D5649475CAD2FC5511B57F3D9BE4645696AE2D6617ADF88FE0F92F76080E54671A333F3C5DB55B377BDE5961DB16EF5349"


def get_user_profile(user_id, access_token):
    url = f"https://np.nuvamawealth.com/edelmw-login/login/user/vendordetails/{user_id}"
    payload = {}
    headers = {
        'Authorization': access_token
    }
    response = req.request("GET", url, headers=headers, data=payload)
    print(response.json())
    return response.json()


get_user_profile(user_id=USER_ID, access_token=access_token)
